// swift-tools-version: 5.9
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "YouboraIMAAdapter",
    platforms: [
        .iOS(.v12),
        .tvOS(.v14),
    ],
    products: [
        .library(
            name: "YouboraIMAAdapter",
            targets: ["YouboraIMAAdapter"]),
    ],
    dependencies: [
        .package(url: "https://bitbucket.org/npaw/lib-plugin-spm-ios.git", .upToNextMajor(from: "6.7.10")),
        .package(url: "https://github.com/googleads/swift-package-manager-google-interactive-media-ads-ios", .upToNextMajor(from: "3.22.0"))
    ],
    targets: [
        .target(name: "YouboraIMAAdapter",
                dependencies: [
                    .product(name: "YouboraLib", package: "lib-plugin-spm-ios"),
                    .product(name: "GoogleInteractiveMediaAds", package: "swift-package-manager-google-interactive-media-ads-ios"),
                ],
                exclude: [
                    "Info.plist"
                ]
        ),
    ]
)
