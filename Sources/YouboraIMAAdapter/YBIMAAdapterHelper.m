//
//  YBIMAAdapterHelper.m
//  
//
//  Created by Josep Bernad on 28/8/24.
//

#import <UIKit/UIKit.h>
//#import "IMAAdsLoaderDelegate.h"
//#import "YBPlugin.h"
#import "YBIMAAdapterSwiftTransformer.h"
#import "YBIMAAdapter.h"
#import "YBIMADAIAdapter.h"

@interface YBIMAAdapterHelper : NSObject <IMAAdsLoaderDelegate>

@property (nonatomic, strong) NSMutableArray<id<IMAAdsLoaderDelegate>> *delegates;
@property (nonatomic, strong) IMAAdsLoader *adsLoader;
@property (nonatomic, strong) YBPlugin *plugin;

- (instancetype)initWithAdsLoader:(IMAAdsLoader *)loader andPlugin:(YBPlugin *)plugin;

@end

@implementation YBIMAAdapterHelper

- (instancetype)initWithAdsLoader:(IMAAdsLoader *)loader andPlugin:(YBPlugin *)plugin {
    self = [super init];
    if (self) {
        self.adsLoader = loader;
        self.plugin = plugin;
        self.delegates = [NSMutableArray array];
        if (self.adsLoader) {
            [self.delegates addObject:self.adsLoader.delegate];
            self.adsLoader.delegate = self;
        }
    }
    return self;
}

- (void)adsLoader:(nonnull IMAAdsLoader *)loader adsLoadedWithData:(nonnull IMAAdsLoadedData *)adsLoadedData {
    for (id<IMAAdsLoaderDelegate> delegate in self.delegates) {
        [delegate adsLoader:loader adsLoadedWithData:adsLoadedData];
    }
    
    if (!self.plugin) {
        return;
    }
    
    if (adsLoadedData.adsManager) {
        self.plugin.adsAdapter = [YBIMAAdapterSwiftTransformer tranformImaAdapter:[[YBIMAAdapter alloc] initWithPlayer:adsLoadedData.adsManager]];
        [self.plugin.adsAdapter fireAdManifest:@{}];
    }
    
    if (adsLoadedData.streamManager) {
        self.plugin.adsAdapter = [YBIMAAdapterSwiftTransformer tranformImaDaiAdapter:[[YBIMADAIAdapter alloc] initWithPlayer:adsLoadedData.streamManager]];
        [self.plugin.adsAdapter fireAdManifest:@{}];
    }
}

- (void)adsLoader:(nonnull IMAAdsLoader *)loader failedWithErrorData:(nonnull IMAAdLoadingErrorData *)adErrorData {
    for (id<IMAAdsLoaderDelegate> delegate in self.delegates) {
        [delegate adsLoader:loader failedWithErrorData:adErrorData];
        
        YBAdManifestError errorType = YBAdManifestErrorNoResponse;
        
        switch (adErrorData.adError.code) {
            case kIMAError_VAST_EMPTY_RESPONSE:
                errorType = YBAdManifestErrorNoResponse;
                break;
            case kIMAError_VAST_TRAFFICKING_ERROR:
            case kIMAError_UNKNOWN_ERROR:
            case kIMAError_VAST_LOAD_TIMEOUT:
                errorType = YBAdManifestErrorNoResponse;
                break;
            case kIMAError_API_ERROR:
            case kIMAError_FAILED_TO_REQUEST_ADS:
            case kIMAError_PLAYLIST_MALFORMED_RESPONSE:
            case kIMAError_VAST_ASSET_NOT_FOUND:
                errorType = YBAdManifestWrongResponse;
                break;
            default:
                errorType = YBAdManifestErrorNoResponse;
                break;
        }
        
        [self.plugin.adsAdapter fireAdManifestWithError:errorType andMessage:adErrorData.adError.message];
    }
}

@end
